<%-- 
    Document   : index
    Created on : 30.08.2022, 21:24:51
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <div id="main">
            <aside class="leftAside">
                <h2>Жанры</h2>
                <ul>
                    <li><a href="#">Фэнтези</a></li>
                    <li><a href="#">Детективы</a></li>
                    <li><a href="#">Научная фантастика</a></li>
                    <li><a href="#">Исторические</a></li>
                    
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Роберт Джордан. Колесо времени. Книга 1</h1>
                    <div class="text-article">
                        С визита необычных незнакомцев начинается для трех друзей путешествие, которое перевернет всю их жизнь, заставит многому научиться и открыть в себе.
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Добавил: <a href="#">автор</a></span>
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата статьи: 20.07.2022</span>
                    </div>
                </article>
                <article>
                    <h1>Евгений Замятин. Мы</h1>
                    <div class="text-article">
                        Е.И. Замятина по праву считают родоначальником литературной антиутопии; спустя четверть века, британский писатель Джордж Оруэлл, вдохновленный работой своего предшественника, написал легендарные «1984» и «Скотный Двор».
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Добавил: <a href="#">автор</a></span>
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата добавления 20.07.2022</span>
                    </div>
                </article>
            </section>
        </div>